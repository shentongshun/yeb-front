let proxyObj = {};

proxyObj["/"] = {
  //websocket
  ws: false,

  //目标地址
  target: "http://localhost:8081",

  //发送请求头会被设置为target的请求头
  changeOrigin: true,

  //前缀路径不重写 斜杠前面没有前缀
  pathReWrite: {
    "^/": "/",
  },
};

module.exports = {
  devServer: {
    host: "localhost",
    port: 8080,
    proxy: proxyObj,
  },
};
